#!/bin/bash

_iso=$1

if [ -z `echo ${_iso} | grep -i iso$` ]
then
  echo "Error: no iso image."
  exit 1
fi

n=0
while [[ $n -lt 10 ]]; do
  [[ -n $(lsblk | grep sr$n) ]] && break
  (( n++ ))
done

if [[ $n == 10 ]]
then
  echo "Error: no dvd drive."
  exit 1
fi

printf "Writing iso ${_iso} to disk=sr$n.\n"
read -p "Press ENTER to start writing.."
[[ $? != 0 ]] && exit 1

growisofs -Z /dev/sr$n=${_iso} \
  -use-the-force-luke=notray \
  -dvd-compat \
  -speed=6 

[[ $? != 0 ]] && exit 1

read -p "Press Enter to eject."
eject

