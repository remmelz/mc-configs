#!/usr/bin/python3

import os
import sys
import re

special_char = re.compile('[ @!#$%^&*()<>?/\|}{~:]')

for f in os.listdir():

    if not os.path.isfile(f): continue
    if(special_char.search(f) == None): continue 

    ext=''
    if '.' in f:
        ext = '.'+f.split('.')[-1]

    newname = ''
    for s in f.split(' '):
        newname += ''.join(filter(str.isalnum, s))+'_'
    newname = newname[:-(len(ext)+1)]+ext

    f = f.replace("'", "'\\''")
    os.system("mv -v '''"+f+"''' "+newname)


