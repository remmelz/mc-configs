#!/bin/bash

#set -x

_cwd=`dirname ${1}`
_iso=`basename ${1}`

[ -z `echo ${_iso} | grep -i iso$` ] && exit

_tmpdir="_Mounted_${_iso}"

if [ ! -d ${_cwd}/${_tmpdir} ]; then
  mkdir ${_cwd}/${_tmpdir}
  sudo mount -o loop ${_cwd}/${_iso} ${_cwd}/${_tmpdir}
else
  sudo umount ${_cwd}/${_tmpdir}
  rmdir ${_cwd}/${_tmpdir}
fi


